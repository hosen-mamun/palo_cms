import { IMG_BASE_URL } from "../config.json";
export function getImageUrl(src, width, height) {
  let url = `https://via.placeholder.com/${width}x${height}`;
  if (src) {
    url = `${IMG_BASE_URL}/${src}?w=${width}&h=${height}&auto=format%2Ccompress`;
  }
  return url;
}
