import React from "react";
import { NavLink } from "react-router-dom";
import Story from "./story";
import SectionHeadline from "./sectionHeadline";

import { getImageUrl } from "../../utils/helper";

const Grid = ({ stories, sectionName }) => {
  return (
    <div className="grid">
      <SectionHeadline name={sectionName} />
      {stories.map((story, index) => (
        <Story
          key={story.id}
          classes={`grid-item grid-item-${index + 1}`}
          img={
            index === 0
              ? {
                  src: getImageUrl(story["hero-image-s3-key"], 301, 170),
                  alt: story.headline,
                  classes: `image image-1`
                }
              : {}
          }
          content={{
            headline: story.headline,
            classes: `content content-1`,
            slug: story.slug
          }}
        />
      ))}
      <NavLink className="moreNewsButton" to="#">
        আরও খবর
      </NavLink>
    </div>
  );
};

export default Grid;
