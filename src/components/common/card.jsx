import React from "react";
import PropTypes from "prop-types";
import Image from "./image";

const Card = ({ img, title, excerpt, tag, posted_time }) => {
  return (
    <div className="card">
      {img && <Image src={img.src} alt={img.alt} />}
      <div className="card-info">
        <h4>{title}</h4>
        {excerpt && <p>{excerpt}</p>}
        {tag && posted_time && (
          <div className="card-info-footer">
            <span>{tag}</span>
            <span>{posted_time}</span>
          </div>
        )}
      </div>
    </div>
  );
};
Card.propTypes = {
  img: PropTypes.object,
  title: PropTypes.string,
  excerpt: PropTypes.string
};
export default Card;
