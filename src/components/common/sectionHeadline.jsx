import React from "react";

const SectionHeadline = ({ name }) => {
  return <h3>{name}</h3>;
};

export default SectionHeadline;
