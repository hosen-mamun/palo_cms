import React from "react";

const Image = ({ ...rest }) => {
  return <img {...rest}></img>;
};

export default Image;
