import React from "react";
import { NavLink } from "react-router-dom";
import { ENDPOINT } from "../../config.json";
import PropTypes from "prop-types";
import Image from "./image";

const Story = ({ img, content, classes }) => {
  return (
    <div className={classes}>
      {img.src && (
        <div className={img.classes}>
          <Image src={img.src} alt={img.alt} />
        </div>
      )}
      <div className={content.classes}>
        <div className="content-title">
          <h4>
            <NavLink to={`${content.slug}`}>{content.headline}</NavLink>
          </h4>
        </div>
        {content.excerpt && (
          <div className="content-excerpt">
            <p>{content.excerpt}</p>
          </div>
        )}
        {content.posted_time && (
          <div className="content-time-tag">
            <p> {content.tag} </p>
            <p>{content.posted_time} </p>
          </div>
        )}
      </div>
    </div>
  );
};
Story.propTypes = {
  img: PropTypes.object,
  content: PropTypes.object
};
export default Story;
