import React from "react";
import { NavLink } from "react-router-dom";
import Image from "./image";

const Gallery = ({ img, title, slug, classes }) => {
  return (
    <div className={classes}>
      <Image src={img.src} alt={img.alt} />
      <h4>
        <NavLink to={`${slug}`}>
          <i className="fa fa-file-image-o" aria-hidden="true"></i>
          &nbsp;{title}
        </NavLink>
      </h4>
    </div>
  );
};

export default Gallery;
