import React from "react";
import Image from "./common/image";
import Logo from "../palo_logo.png";

const TopHeader = () => {
  return (
    <div className="top-header">
      <div className="top-header-side"></div>
      <div className="top-header-center">
        <Image src={Logo} alt={"Prothom Alo Logo"} />
      </div>
      <div className="top-header-side"></div>
    </div>
  );
};

export default TopHeader;
