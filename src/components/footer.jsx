import React from "react";
import logo from "../palo_155.png";
import "font-awesome/css/font-awesome.min.css";

const Footer = () => {
  return (
    <footer>
      <section className="ft-main">
        <div className="ft-main-item left-side">
          <img src={logo} alt="palo 155x35" />
          <div className="ft-links">
            <ul>
              <li>
                <a href="">মোবাইল ভ্যাস</a>
              </li>
              <li>
                <a href="">ট্রাস্ট</a>
              </li>
              <li>
                <a href="">প্রতিচিন্তা</a>
              </li>
              <li>
                <a href="">কিশোর আলো</a>
              </li>
              <li>
                <a href="">এবিসি রেডিও</a>
              </li>
              <li>
                <a href="">বন্ধুসভা</a>
              </li>
              <li>
                <a href="">প্রথমা</a>
              </li>
            </ul>
          </div>
          <div className="ft-social">
            <ul>
              <li>
                <a href="">
                  <i className="fa fa-facebook" aria-hidden="true"></i>{" "}
                </a>
              </li>
              <li>
                <a href="">
                  <i className="fa fa-twitter" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a href="">
                  <i className="fa fa-instagram" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a href="">
                  <i className="fa fa-pinterest-p" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a href="">
                  <i className="fa fa-youtube-play" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="ft-main-item right-side">
          <p>প্রথম আলো নিউজ লেটার</p>
          <div className="subscribe">
            <input
              type="email"
              name="email"
              placeholder="আপনার ইমেইল আইডি দিন"
            />
            <input type="submit" value="সাবস্ক্রাইব" />
          </div>
          <div className="download-link">
            <p>মোবাইল অ্যাপস ডাউনলোড করুন</p>
          </div>
        </div>
      </section>

      <section className="ft-bottom">
        <p className="copy-right">© প্রথম আলো ২০১৯</p>
        <ul className="legal">
          <li>
            <a href="">Terms & Condition</a>
          </li>
          <li>
            <a href="">Privacy Policy</a>
          </li>
          <li>
            <a href="">About</a>
          </li>
          <li>
            <a href="">Advertise</a>
          </li>
          <li>
            <a href="">Advertise</a>
          </li>
        </ul>
        <p className="editor">সম্পাদক ও প্রকাশক: মতিউর রহমান</p>
      </section>
    </footer>
  );
};

export default Footer;
