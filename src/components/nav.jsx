import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import Navlist from "../navs.json";

const Nav = () => {
  /* I assumed navs list will be populated from DB*/
  const [navs, setNav] = useState([]);
  useEffect(() => {
    setNav(Navlist.navs);
  }, []);

  return (
    <nav>
      {navs &&
        navs.map(nav => (
          <NavLink to={nav.to} key={nav.id}>
            {nav.name}
          </NavLink>
        ))}
    </nav>
  );
};

export default Nav;
