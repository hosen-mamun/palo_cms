import React, { useState, useEffect } from "react";
import { getStoriesAccordingToSection } from "../../services/storyService";
import SectionHeadline from "../common/sectionHeadline";
import Story from "../common/story";
import { getImageUrl } from "../../utils/helper";
import "../../css/home/rowEleven.css";

const RowFive = () => {
  const imageSizes = [
    { width: 622, height: 291 },
    { width: 194, height: 291 },
    { width: 194, height: 291 },
    { width: 194, height: 291 }
  ];
  const [stories, setStories] = useState([]);
  useEffect(() => {
    async function getData() {
      const { data } = await getStoriesAccordingToSection(11, 4, 45);
      setStories(data.stories);
    }
    getData();
  }, []);
  return (
    <React.Fragment>
      {stories && (
        <section id="row-eleven">
          <SectionHeadline name={"বিজ্ঞান ও প্রযুক্তি"} />
          <div className="grid">
            {stories.map((story, index) => (
              <Story
                key={story.id}
                classes={`grid-item grid-item-${index + 1}`}
                img={{
                  src: getImageUrl(
                    story["hero-image-s3-key"],
                    imageSizes[index].width,
                    imageSizes[index].height
                  ),
                  alt: story.headline,
                  classes: `image image-1`
                }}
                content={{
                  headline: story.headline,
                  excerpt: story.subheadline,
                  slug: story.slug,
                  tag: "বাংলা গান",
                  posted_time: "১৬ মিনিট আগে ",
                  classes: `content content-1`
                }}
              />
            ))}
          </div>
        </section>
      )}
    </React.Fragment>
  );
};

export default RowFive;
