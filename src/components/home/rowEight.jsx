import React, { useState, useEffect } from "react";
import { getStoriesAccordingToSection } from "../../services/storyService";
import SectionHeadline from "../common/sectionHeadline";
import Article from "../common/article";
import { IMG_BASE_URL } from "../../config.json";
import "../../css/home/rowEight.css";

const RowEight = () => {
  const [stories, setStories] = useState([]);
  useEffect(() => {
    async function getData() {
      const { data } = await getStoriesAccordingToSection(8, 4, 40);
      setStories(data.stories);
    }
    getData();
  }, []);
  return (
    <React.Fragment>
      {stories && (
        <section id="row-eight">
          <SectionHeadline name={"অর্থনীতি"} />
          <div className="grid">
            <div className="grid-item grid-item-1">
              {stories.slice(0, 1).map((story, index) => (
                <Article
                  key={story.id}
                  img={{
                    src: story["hero-image-s3-key"]
                      ? `${IMG_BASE_URL}/${story["hero-image-s3-key"]}`
                      : "https://via.placeholder.com/622x350",
                    alt: story.headline,
                    classes: `image image${index + 1}`
                  }}
                  content={{
                    headline: story.headline,
                    slug: story.slug,
                    excerpt: story.subheadline,
                    classes: `content content${index + 1}`,
                    tag: "বাংলা গান",
                    posted_time: "১৬ মিনিট আগে "
                  }}
                />
              ))}
            </div>
            <div className="grid-item grid-item-2">
              {stories.slice(1, 4).map((story, index) => (
                <Article
                  key={story.id}
                  img={{
                    src: story["hero-image-s3-key"]
                      ? `${IMG_BASE_URL}/${story["hero-image-s3-key"]}`
                      : "https://via.placeholder.com/220x147",
                    alt: story.headline,
                    classes: `image image${index + 1}`
                  }}
                  content={{
                    headline: story.headline,
                    excerpt: story.subheadline,
                    classes: `content content${index + 1}`,
                    tag: "বাংলা গান",
                    posted_time: "১৬ মিনিট আগে "
                  }}
                />
              ))}
            </div>
          </div>
        </section>
      )}
    </React.Fragment>
  );
};

export default RowEight;
