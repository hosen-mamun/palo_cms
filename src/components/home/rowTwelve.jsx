import React, { useState, useEffect } from "react";
import { getStoriesAccordingToSection } from "../../services/storyService";
import SectionHeadline from "../common/sectionHeadline";
import Gallery from "../common/gallery";
import { getImageUrl } from "../../utils/helper";
import "../../css/home/rowTwelve.css";

const RowTwelve = () => {
  const imageSizes = [
    { width: 622, height: 622 },
    { width: 622, height: 304 },
    { width: 301, height: 301 },
    { width: 301, height: 301 }
  ];
  const [stories, setStories] = useState([]);
  useEffect(() => {
    async function getData() {
      const { data } = await getStoriesAccordingToSection(12, 4, 50);
      setStories(data.stories);
    }
    getData();
  }, []);
  return (
    <React.Fragment>
      {stories && (
        <section id="row-twelve">
          <SectionHeadline name={"ছবি"} />
          <div className="grid">
            {stories.slice(0, 1).map((gallery, index) => (
              <Gallery
                key={gallery.id}
                img={{
                  src: getImageUrl(
                    gallery["hero-image-s3-key"],
                    imageSizes[index].width,
                    imageSizes[index].height
                  ),
                  alt: gallery.headline
                }}
                title={gallery.headline}
                slug={gallery.slug}
                classes={`grid-item grid-item-${index + 1}`}
              />
            ))}
            <div className="grid-item grid-item-2">
              {stories.slice(1, 4).map((gallery, index) => (
                <Gallery
                  key={gallery.id}
                  img={{
                    src: getImageUrl(
                      gallery["hero-image-s3-key"],
                      imageSizes[index + 1].width,
                      imageSizes[index + 1].height
                    ),
                    alt: gallery.headline
                  }}
                  title={gallery.headline}
                  slug={gallery.slug}
                  classes={`grid-item grid-item-2-${index + 1}`}
                />
              ))}
            </div>
          </div>
        </section>
      )}
    </React.Fragment>
  );
};

export default RowTwelve;
