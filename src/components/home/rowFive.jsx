import React, { useState, useEffect } from "react";
import { getStoriesAccordingToSection } from "../../services/storyService";
import SectionHeadline from "../common/sectionHeadline";
import Story from "../common/story";
import { getImageUrl } from "../../utils/helper";
import "../../css/home/rowFive.css";

const RowFive = () => {
  const [stories, setStories] = useState([]);
  useEffect(() => {
    async function getData() {
      const { data } = await getStoriesAccordingToSection(5, 4, 20);
      setStories(data.stories);
    }
    getData();
  }, []);
  return (
    <React.Fragment>
      {stories && (
        <section id="row-five">
          <SectionHeadline name={"আমার পছন্দ"} />
          <div className="grid">
            {stories.slice(0, 4).map((story, index) => (
              <Story
                key={story.id}
                classes={`grid-item grid-item-${index + 1}`}
                img={{
                  src: getImageUrl(story["hero-image-s3-key"], 301, 201),
                  alt: story.headline,
                  classes: `image image-1`
                }}
                content={{
                  headline: story.headline,
                  slug: story.slug,
                  excerpt: story.subheadline,
                  classes: `content content-1`
                }}
              />
            ))}
          </div>
        </section>
      )}
    </React.Fragment>
  );
};

export default RowFive;
