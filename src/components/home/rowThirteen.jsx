import React, { useState, useEffect } from "react";
import { getStoriesAccordingToSection } from "../../services/storyService";
import Grid from "../common/grid";
import "../../css/home/rowThirteen.css";

const RowThirteen = () => {
  const [stories, setStories] = useState([]);
  useEffect(() => {
    async function getData() {
      const { data } = await getStoriesAccordingToSection(13, 12, 40);
      setStories(data.stories);
    }
    getData();
  }, []);
  return (
    <React.Fragment>
      {stories && (
        <section id="row-thirteen">
          <Grid stories={stories.slice(0, 3)} sectionName="আন্তর্জাতিক" />
          <Grid stories={stories.slice(3, 6)} sectionName="জীবনযাপন" />
          <Grid stories={stories.slice(6, 9)} sectionName="উত্তর আমেরিকা" />
          <Grid stories={stories.slice(9, 12)} sectionName="ENGLISH" />
        </section>
      )}
    </React.Fragment>
  );
};

export default RowThirteen;
