import React from "react";
import RowFive from "./rowFive";
import RowSix from "./rowSix";
import RowEight from "./rowEight";
import RowEleven from "./rowEleven";
import RowTwelve from "./rowTwelve";
import RowThirteen from "./rowThirteen";

const Home = () => {
  return (
    <React.Fragment>
      <RowFive />
      <RowSix />
      <RowEight />
      <RowEleven />
      <RowTwelve />
      <RowThirteen />
    </React.Fragment>
  );
};
export default Home;
