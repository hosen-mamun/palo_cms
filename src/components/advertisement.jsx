import React from "react";
import Image from "./common/image";

const Advertisement = ({ ...rest }) => {
  return (
    <div className="advertisement">
      <Image {...rest} />
    </div>
  );
};

export default Advertisement;
