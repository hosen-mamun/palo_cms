import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Home from "./home/home";
import NotFound from "./notFound";

class Main extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route path="/not-found" component={NotFound}></Route>
        <Redirect to="/not-found" />
      </Switch>
    );
  }
}

export default Main;
