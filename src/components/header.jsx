import React from "react";
import Nav from "./nav";
import TopHeader from "./topHeader";

const Header = () => {
  return (
    <header>
      <TopHeader />
      <Nav />
    </header>
  );
};

export default Header;
