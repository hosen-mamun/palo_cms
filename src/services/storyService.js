import http from "./httpService";
import { API_URL } from "../config.json";
export function getStories() {
  const apiEndPoint = `${API_URL}/stories?fields=id,headline,subheadline,slug,sections,tags,author-name,authors,hero-image-s3-key,hero-image-metadata,published-at,story-template`;
  return http.get(apiEndPoint);
}

export function getStoriesAccordingToSection(section_id, limit, offset) {
  const apiEndPoint = `${API_URL}/stories?fields=id,headline,subheadline,slug,sections,tags,author-name,authors,hero-image-s3-key,hero-image-metadata,published-at,story-template&section_id=${section_id}&limit=${limit}&offset=${offset}`;
  return http.get(apiEndPoint);
}
