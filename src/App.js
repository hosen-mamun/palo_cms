import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import AdvT1 from "./components/advertisement";
import Header from "./components/header";
import Footer from "./components/footer";
import AdvImg from "./t1.gif";
import "./App.css";

function App() {
  const Main = lazy(() => import("./components/main"));
  return (
    <React.Fragment>
      <Router>
        <AdvT1 src={AdvImg} alt={"advertisement"} />
        <Header />
        <Suspense fallback={<h1>Loading…</h1>}>
          <Main />
        </Suspense>
        <Footer />
      </Router>
    </React.Fragment>
  );
}

export default App;
